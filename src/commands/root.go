package commands

import (
	"github.com/spf13/cobra"
	"os"
	"fmt"
)


var logo = `
 _      _                 _____  _       _____ 
| |    (_)               / ____|| |     |_   _|
| |__   _  _ __    ___  | |     | |       | |  
| '_ \ | || '_ \  / _ \ | |     | |       | |  
| |_) || || | | || (_) || |____ | |____  _| |_ 
|_.__/ |_||_| |_| \___/  \_____||______||_____|
`

var rootCmd = &cobra.Command{
	Use:   "bino",
	Short: "Bino é uma interface de Linha de Comando do Trizy",
	Long: logo,
}
  
func Execute() {
	if err := rootCmd.Execute(); err != nil {
	  fmt.Println(err)
	  os.Exit(1)
	}
}