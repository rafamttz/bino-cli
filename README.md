# Bino CLI #


### Para que serve esse repo? ###

Interface de Linha de Comando para time de desenvolvimento da Trizy voltada na criação de ambientes de desenvolvimento.
*Versão 0.0.0

### Como eu faço o set up? ###

* Instalar go versão ^1.13
* No diretório raiz digite `go run src/bino.go`

Para fazer o build da ferramenta fazemos:
`go build src/bino.go`

O comando build irá gerar o programa executável e para usar basta usar o comando `./bino [comando]`.

### Como contribuir? ###

* Entrar em contato com o time de Arquitetura e Sustentação da Trizy

### Com quem eu devo falar? ###

* Anderson Luiz Pancheniak (anderson.pancheniak@trizy.com.br)
* Fábio Castro (fabio.castro@trizy.com.br)
* Rafael de Mattos (rafael.mattos@trizy.com.br)