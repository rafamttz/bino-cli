package commands

import (
	"fmt"
	"github.com/spf13/cobra"
)

// Adicionando comandos linkados com o comando root
func init() {
	rootCmd.AddCommand(versionCmd)
}

// Declarando o funcionamento do comando
var versionCmd = &cobra.Command {
	Use: "version",
	Short: "Versão do Bino CLI",
	Long: "Descreve a versão atual do Bino CLI instalado no seu computador",
	Run: func (cmd *cobra.Command, args []string)  {
		fmt.Println("Bino CLI v0.0.0")
	},
}
